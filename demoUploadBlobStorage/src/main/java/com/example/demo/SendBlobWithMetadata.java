package com.example.demo;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.HashMap;
import java.util.UUID;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.OperationContext;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.BlobContainerPublicAccessType;
import com.microsoft.azure.storage.blob.BlobRequestOptions;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;


public class SendBlobWithMetadata {
	
	
	public static final String STORAGE_ACCOUNT_NAME = "alicorpazexample001";
	public static final String STORAGE_ACCOUNT_KEY = "KRxbSJbcuY0M3rwIWp7gekzz4QlPoy6tiJebpCEPXnmgoseIn+BV2foVx9x/mtxcp0iYLgGOHbUtSVv7W16VMA==";
	public static final String STORAGE_CONTAINER_NAME = "example";
	public static final String STORAGE_CONNECTION_STRING = "DefaultEndpointsProtocol=https;" + "AccountName="
			+ STORAGE_ACCOUNT_NAME + ";" + "AccountKey=" + STORAGE_ACCOUNT_KEY;

	public static void main(String[] args) {
		HashMap<String, String> metadata = new HashMap<String, String>();
		metadata.put("useridentifier", "javier.perez@everis.com");
		metadata.put("description", "Example for metadata");
		metadata.put("operation", "RECAUDO-BATCH-SIN-REFERENCIA");

		File sourceFile = null;
		System.out.println("Azure Blob storage quick start sample");

		CloudBlobContainer container = null;

		try {
			container=getBlobContainer();
			
			sourceFile=getTempFile();
			
			CloudBlockBlob blob = container.getBlockBlobReference(sourceFile.getName());
			blob.setMetadata(metadata);
			System.out.println("Uploading the sample file ");
			blob.uploadFromFile(sourceFile.getAbsolutePath());
			
		} catch (StorageException ex) {
			System.out.println(String.format("Error returned from the service. Http code: %d and error code: %s",
					ex.getHttpStatusCode(), ex.getErrorCode()));
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		} finally {
			System.out.println("The program has completed successfully.");
		}
	}
	
	private static CloudBlobContainer getBlobContainer() throws InvalidKeyException, URISyntaxException, StorageException {
		CloudStorageAccount storageAccount;
		CloudBlobClient blobClient = null;
		CloudBlobContainer container = null;
		
		storageAccount = CloudStorageAccount.parse(STORAGE_CONNECTION_STRING);
		blobClient = storageAccount.createCloudBlobClient();
		container = blobClient.getContainerReference(STORAGE_CONTAINER_NAME);

		System.out.println("Creating container: " + container.getName());
		container.createIfNotExists(BlobContainerPublicAccessType.CONTAINER, new BlobRequestOptions(),
				new OperationContext());
		return container;
	}
	
	private static File getTempFile() throws IOException {
		File tempFile = null;
		tempFile = File.createTempFile("trx-" + UUID.randomUUID().toString(), ".txt");

		System.out.println("Creating a sample file at: " + tempFile.toString());

		Writer output = new BufferedWriter(new FileWriter(tempFile));
		output.write("Line 1\n");
		output.write("Line 2\\n");
		output.write("Line 3\\n");
		output.write("Line 4");
		output.close();
		return tempFile;
	}


}
